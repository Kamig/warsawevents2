﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    class InvitationsControllerTests
    {
        [Test]
        public void PostInvitation()
        {
            IRepository<Invitation> rep = new MockRepository<Invitation>();
            Invitation invitation = new Invitation() { SendUserId = 0, ReceiveUserId = 9 };


            var invitationsController = new InvitationsController(rep);
            invitationsController.Post(invitation);


            List<Invitation> invitations = rep.GetAll().ToList();

            Assert.AreEqual(true, invitations.Any((Invitation i) => i.SendUserId == invitation.SendUserId && i.ReceiveUserId == invitation.ReceiveUserId));
        }
    }
}
