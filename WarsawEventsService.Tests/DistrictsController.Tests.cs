﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    class DistrictsControllerTests
    {
        [Test]
        public void GetAllDistricts()
        {
            IRepository<District> rep = new MockRepository<District>();
            District district1 = new District() { Id = 0, DistrictName = "Śródmieście" };
            District district2 = new District() { Id = 1, DistrictName = "Wola" };
            rep.Insert(district1);
            rep.Insert(district2);
            var districtsController = new DistrictsController(rep);

            var actionResult = districtsController.GetAll();

            ICollection<District> districts = (actionResult as OkNegotiatedContentResult<ICollection<District>>).Content;

            Assert.AreEqual(true, districts.Any((District d) => d.Id == district1.Id && d.DistrictName == district1.DistrictName));
            Assert.AreEqual(true, districts.Any((District d) => d.Id == district2.Id && d.DistrictName == district2.DistrictName));
        }

        [Test]
        public void GetDistrictById()
        {
            IRepository<District> rep = new MockRepository<District>();
            District district1 = new District() { Id = 0, DistrictName = "Śródmieście" };
            District district2 = new District() { Id = 1, DistrictName = "Wola" };
            rep.Insert(district1);
            rep.Insert(district2);
            var districtsController = new DistrictsController(rep);

            var actionResult = districtsController.GetById(0);

            District district = (actionResult as OkNegotiatedContentResult<District>).Content;

            Assert.AreEqual(0, district.Id);

        }
    }
}
