﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    class CommentsControllerTests
    {

            [Test]
            public void PostComment()
            {
                IRepository<Comment> rep = new MockRepository<Comment>();
                Comment comment = new Comment() { Id = 0 };

                var commentsController = new CommentsController(rep);
                commentsController.Post(comment);


                List<Comment> comments = rep.GetAll().ToList();

                Assert.AreEqual(true, comments.Any((Comment c) => c.Id == comment.Id));
            }
        
    }
}
