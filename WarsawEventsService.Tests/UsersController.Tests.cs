﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Linq;
using System.Threading.Tasks;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    public class UsersControllerTest
    {

        [Test]
        public void GetAllUsers()
        {
                IRepository<User> rep = new MockRepository<User>();
                User user1 = new User() { Id = 0, UserName = "User1" };
                User user2 = new User() { Id = 1, UserName = "User2" };
                rep.Insert(user1);
                rep.Insert(user2);
                var usersController = new UsersController(rep);
           
                var actionResult = usersController.GetAll();

                IEnumerable<User> users = (actionResult as OkNegotiatedContentResult< IEnumerable<User> >).Content;

                Assert.AreEqual(true, users.Any((User u) => u.Id == user1.Id && u.UserName == user1.UserName));
                Assert.AreEqual(true, users.Any((User u) => u.Id == user2.Id && u.UserName == user2.UserName));
        }

        [Test]
        public void GetUserByLogin()
        {
            IRepository<User> rep = new MockRepository<User>();
            User user1 = new User() { Id = 0, UserName = "User1" };
            rep.Insert(user1);
            var usersController = new UsersController(rep);

            var actionResult = usersController.GetByLogin("User1");

            User user = (actionResult as OkNegotiatedContentResult<User>).Content;

            Assert.AreEqual(0, user.Id);

        }
    }
}
