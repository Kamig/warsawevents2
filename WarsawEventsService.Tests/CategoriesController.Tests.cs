﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    public class CategoryiesControllerTests
    {
        [Test]
        public void GetAllCategories()
        {
            IRepository<Category> rep = new MockRepository<Category>();
            Category category1 = new Category() { Id = 0, CategoryName = "Nauka" };

            Category category2 = new Category() { Id = 1, CategoryName = "Sport" };
            rep.Insert(category1);
            rep.Insert(category2);
            var categoriesController = new CategoriesController(rep);

            var actionResult = categoriesController.GetAll();

            ICollection<Category> categories = (actionResult as OkNegotiatedContentResult<ICollection<Category>>).Content;

            Assert.AreEqual(true, categories.Any((Category c) => c.Id == category1.Id && c.CategoryName == category1.CategoryName));
            Assert.AreEqual(true, categories.Any((Category c) => c.Id == category2.Id && c.CategoryName == category2.CategoryName));
        }

        [Test]
        public void GetCategoryById()
        {
            IRepository<Category> rep = new MockRepository<Category>();
            Category category1 = new Category() { Id = 0, CategoryName = "Nauka" };

            Category category2 = new Category() { Id = 1, CategoryName = "Sport" };
            rep.Insert(category1);
            rep.Insert(category2);
            var categoriesController = new CategoriesController(rep);

            var actionResult = categoriesController.GetById(0);

            Category category = (actionResult as OkNegotiatedContentResult<Category>).Content;

            Assert.AreEqual(0, category.Id);

        }
    }
}
