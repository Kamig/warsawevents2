﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using WarsawEventsService.Controllers;

namespace WarsawEventsService.Tests
{
    [TestFixture]
    public class PlacesControllerTests
    {
        [Test]
        public void GetPlaceByName()
        {
            IRepository<Place> rep = new MockRepository<Place>();
            District srodmiescie = new District() { DistrictName = "Śródmieście" };
            Place place = new Place() { Id = 0, District = srodmiescie, PlaceName = "Klub",
                StreetName = "Marszałkowska", Streetnumber = 69, Grades = new List<Grade>() };
            rep.Insert(place);
            var placesController = new PlacesController(rep);

            var actionResult = placesController.GetByName(place.PlaceName);

            List<Place> places = (actionResult as OkNegotiatedContentResult<List<Place>>).Content;

            Assert.AreEqual(true, places.Any((Place p) => p.Id == place.Id));
        }

        [Test]
        public void GetPlaceByDistrictName()
        {
            IRepository<Place> rep = new MockRepository<Place>();
            District srodmiescie = new District() { DistrictName = "Śródmieście" };
            Place place = new Place()
            {
                Id = 0,
                District = srodmiescie,
                PlaceName = "Klub",
                StreetName = "Marszałkowska",
                Streetnumber = 69,
                Grades = new List<Grade>()
            };

            Place place1 = new Place()
            {
                Id = 0,
                District = new District() { DistrictName="Wola" },
                PlaceName = "Klub",
                StreetName = "Marszałkowska",
                Streetnumber = 69,
                Grades = new List<Grade>()
            };

            Place place2 = new Place()
            {
                Id = 0,
                District = srodmiescie,
                PlaceName = "Klub",
                StreetName = "Warszawska",
                Streetnumber = 45,
                Grades = new List<Grade>()
            };
            rep.Insert(place);
            rep.Insert(place2);
            var placesController = new PlacesController(rep);

            var actionResult = placesController.GetByName(srodmiescie.DistrictName);

            List<Place> places = (actionResult as OkNegotiatedContentResult<List<Place>>).Content;

            Assert.AreEqual(true, places.Count==2);
        }

        [Test]
        public void PostPlace()
        {
            IRepository<Place> rep = new MockRepository<Place>();
            District srodmiescie = new District() { DistrictName = "Śródmieście" };
            Place place = new Place()
            {
                Id = 0,
                District = srodmiescie,
                PlaceName = "Klub",
                StreetName = "Marszałkowska",
                Streetnumber = 69,
                Grades = new List<Grade>()
            };
            
            var placesController = new PlacesController(rep);
            placesController.Post(place);


            List<Place> places = rep.GetAll().ToList();

            Assert.AreEqual(true, places.Any((Place p) => p.Id == place.Id));
        }
    }
}
