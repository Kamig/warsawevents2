﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class GradesController : ApiController
    {

        IRepository<Grade> gradesRepository;
        IRepository<Place> placesRepository;

        public GradesController(IRepository<Grade> gradesRepository, IRepository<Place> placesRepository)
        {
            this.gradesRepository = gradesRepository;
            this.placesRepository = placesRepository;
        }

        //[HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]Grade grade)
        {
            WarsawEventsContext context = new WarsawEventsContext();
            //using (Repository<Grade> gradesRepository = new Repository<Grade>(context))
           // {
                Grade old = gradesRepository.Get().Where(g => g.PlaceId == grade.PlaceId && g.UserId == grade.UserId).FirstOrDefault();
                if(old!=null) //uzytkownik ocenil juz to miejsce
                    return BadRequest();
                gradesRepository.Insert(grade);
                gradesRepository.Save();
                return Ok( UpdatePlaceAverage(grade.PlaceId));
            //}
        }

        private Place UpdatePlaceAverage(int id)
        {
            //using (Repository<Place> placeRepository = new Repository<Place>(new WarsawEventsContext()))
            //{
                Place place = placesRepository.Get().Where(p=>p.Id==id).Include(p=>p.Grades).FirstOrDefault();
                int sum = 0;
                foreach(var g in place.Grades)
                {
                    sum += g.Value;
                }
                place.Average = (double)sum / place.Grades.Count;
                placesRepository.Update(place);
                placesRepository.Save();
                return place;
            //}
        }
    }
}
