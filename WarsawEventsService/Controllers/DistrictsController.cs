﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class DistrictsController : ApiController
    {

        IRepository<District> districtRepository;

        public DistrictsController(IRepository<District> districtRepository)
        {
            this.districtRepository = districtRepository;
        }

        //[HttpGet]
        [UseSSL]
        public IHttpActionResult GetAll()
        {
            //using (Repository<District> districtRepository = new Repository<District>(new WarsawEventsContext()))
           // {
                return Ok( districtRepository.GetAll());
           // }
        }

        //[HttpGet]
        [UseSSL]
        public IHttpActionResult GetById(int id)
        {
           // using (Repository<District> districtRepository = new Repository<District>(new WarsawEventsContext()))
           // {
                return Ok(districtRepository.Find(id));
            //}
        }
    }
}
