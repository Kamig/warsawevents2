﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;

namespace WarsawEventsService.Controllers
{
    public class CommentsController : ApiController
    {
        IRepository<Comment> commentRepository;

        public CommentsController(IRepository<Comment> commentRepository)
        {
            this.commentRepository = commentRepository;
        }

        //[HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]Comment comment)
        {

                comment.User = null;
                commentRepository.Insert(comment);
                commentRepository.Save();
                return Ok();
        }
    }
}
