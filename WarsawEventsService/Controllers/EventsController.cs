﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WarsawEvents.DataModel;
using WarsawEvents.DataModel.Entities;
using System.Diagnostics;
using System.Data.Entity;

namespace WarsawEventsService.Controllers
{
    public class EventsController : ApiController
    {
        IEventRepository eventRepository;
        IRepository<Place> placeRepository;
        IRepository<Comment> commentsRepository;
        IRepository<User> userRepository;

        public EventsController(IEventRepository eventRepository, IRepository<Place> placeRepository, 
            IRepository<Comment> commentsRepository, IRepository<User> userRepository)
        {
            this.eventRepository = eventRepository;
            this.placeRepository = placeRepository;
            this.commentsRepository = commentsRepository;
            this.userRepository = userRepository;
        }
            // [HttpGet]
        [UseSSL]
        public IHttpActionResult GetFiltered(string phrase="", string category= "Wszystkie", string district= "Wszystkie", string period= "Wszystkie",
                  string typeOfEvents = "Wszystkie", int UserId = -1, string sort="Dacie", int page=1, int rpp=4 )//rpp - record per page
        {// api/events?category=Nauka&district=Śródmieście&period=Wszystkie&sort=Dacie - przykladowy uri
  
                List<Expression<Func<Event, bool>>> funcList = new List<Expression<Func<Event, bool>>>();

                if(phrase!="")
                    funcList.Add((Event e) => e.EventName.Contains(phrase) || e.Place.PlaceName.Contains(phrase) || e.Place.District.DistrictName.Contains(phrase)
                           || e.Description.Contains(phrase) || e.Place.StreetName.Contains(phrase)
                           || e.Category.CategoryName.Contains(phrase));
                if (category != "Wszystkie")
                    funcList.Add((Event e) => (e.Category.CategoryName == category));
                if (district != "Wszystkie")
                    funcList.Add((Event e) => (e.Place.District.DistrictName == district));
                funcList.Add(FuncTool.GetPeriodFunc(period));

                //Type of events
                User user = userRepository.Get().Where((User u) => u.Id == UserId).
                    Include((u) => u.SendInvitations).Include((u) => u.ReceiveInvitations).FirstOrDefault();

                funcList.Add(FuncTool.GetTypeOfEventsFunc(typeOfEvents, user));

                //IEnumerable<Event> events= eventRepository.GetEvents(funcList).
                //    OrderBy(FuncTool.GetSortFunc(sort)).
                //    Skip(rpp * (page - 1)).
                //    Take(rpp).ToList();

                IEnumerable<Event> events = eventRepository.GetEvents(funcList).
                    Include(e => e.Place).
                    Include(e => e.Category).
                    //Include(e => e.Likes).
                   // Include(e => e.Participants).
                    Include(e => e.Place.District).
                    OrderBy(FuncTool.GetSortFunc(sort)).
                    Skip(rpp * (page - 1)).
                    Take(rpp).ToList();

                if (events == null)
                    return NotFound();
                GetUserNamesToComments(events);
                return Ok(events);
            
        }

        // [HttpGet]
        [UseSSL]
        public IHttpActionResult Get(int id)
        {
   
                return Ok(eventRepository.Find(id));
           
        }

        //[HttpPost]
        [UseSSL]
        public IHttpActionResult Post([FromBody]Event e)
        { // api/events  
    
                Place place = CheckIfPlaceExist(e.Place);
                if (place != null)//juz istnieje takie miejsce
                {
                    e.PlaceId = place.Id;
                    e.Place = null;//bez tego nie smiga
                }
                Event added =  eventRepository.InsertEvent(e);
                eventRepository.Save();
                return Ok(added.Id);
            
        }

        //[HttpPut]
        [UseSSL]
        public IHttpActionResult Put(int id,[FromBody]Event e)
        {
 
                var eventToEdit = eventRepository.Find(id);
                eventToEdit.EventName = e.EventName;
                eventToEdit.Description = e.Description;
                eventToEdit.Price = e.Price;
                eventToEdit.NumberOfParticipants = e.NumberOfParticipants;

                eventToEdit.DateOfStart = e.DateOfStart;
                Place place = CheckIfPlaceExist(e.Place);
                if (place == null)
                {//tworzy nowy place
                    eventToEdit.Place = e.Place;
                    eventToEdit.Place.DistrictId = e.Place.DistrictId;
                }
                else//podpinam isniejace place 
                    eventToEdit.PlaceId = place.Id;
                eventToEdit.CategoryId = e.CategoryId;
                eventRepository.UpdateEvent(eventToEdit);
                eventRepository.Save();//musi byc bo inaczej nie zapisuje zmian
                return Ok();
            
        }

        //[HttpDelete]
        [UseSSL]
        public IHttpActionResult Delete(int id)
        {

                eventRepository.DeleteEvent(id);
                eventRepository.Save();
                return Ok();
        }

        private Place CheckIfPlaceExist(Place place)
        {//jezeli w bazie istnieje taki place to go zwroci

                return placeRepository.Get().Where(p => p.DistrictId == place.DistrictId
                    && p.PlaceName == place.PlaceName && p.StreetName == place.StreetName
                    && p.Streetnumber == place.Streetnumber).FirstOrDefault();
        
        }
        private void GetUserNamesToComments(IEnumerable<Event> events)
        {
   
                foreach (var e in events)
                {
                    e.Comments = commentsRepository.Get().Where(c => c.EventId == e.Id).Include(c => c.User).OrderBy(c=>c.Date).ToList();
                }
                  
                
        }
    }
}
