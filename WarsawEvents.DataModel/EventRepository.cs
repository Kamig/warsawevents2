﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WarsawEvents.DataModel.Entities;

namespace WarsawEvents.DataModel
{
    public interface IEventRepository : IDisposable
    {
        // Event GetEventByID(int eventId);
        //IQueryable<Event> GetEventsByCategory(Category category);
        //IQueryable<Event> GetEventsByDistrict(District district);
        //IQueryable<Event> GetEventsByPeriodOfTime(PeriodOfTime periodOfTime);
        //IQueryable<Event> GetEventsByPrice(int price);
        //IQueryable<Event> GetEventsByPhrase(string phrase);
        IQueryable<Event> GetCurrentEvents();
        Event Find(int id);
        Event InsertEvent(Event newEvent);
        void DeleteEvent(int eventId);
        void UpdateEvent(Event updatedEvent);
        IQueryable<Event> GetEvents(IList<Expression<Func<Event, bool>>> funcList);


        void Save();
    }

    public class EventRepository: IEventRepository
    {
        private WarsawEventsContext context;

        public EventRepository(WarsawEventsContext context)
        {
            this.context = context;
        }
        public void DeleteEvent(int eventId)
        {
            Event concreteEvent = context.Events.Find(eventId);
            context.Events.Remove(concreteEvent);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public Event Find(int id)
        {
            return context.Events.Find(id);
        }

        public IQueryable<Event> GetCurrentEvents()
        {
            var query = from e in context.Events
                        where e.DateOfStart > DateTime.Now
                        select e;

            return query;
        }

        public IQueryable<Event> GetEvents(IList<Expression<Func<Event, bool>>> funcList)
            {
            
            IQueryable<Event> query = from e in context.Events select e;

            foreach (Expression<Func<Event, bool>> func in funcList)
                query = query.Where(func); 

            return query;
        }

        //public Event GetEventByID(int eventId)
        //{
        //    return context.Events.Find(eventId); 
        //}

        //public IQueryable<Event> GetEventsByCategory(Category category)
        //{
        //    var query = from e in context.Events
        //                where e.Category.Id==category.Id //bez .id rzucało wyjątek
        //                select e;

        //    return query;

        //}

        //public IQueryable<Event> GetEventsByDistrict(District district)
        //{
        //    var query = from e in context.Events
        //                where e.Place.District.Id == district.Id //bez .id rzucało wyjątek
        //                select e;

        //    return query;

        //}

        //public IQueryable<Event> GetEventsByPeriodOfTime(PeriodOfTime periodOfTime)
        //{
        //    var query = from e in context.Events
        //                where e.DateOfStart >= periodOfTime.Start && e.DateOfStart <= periodOfTime.End
        //                select e;

        //    return query;
        //}

        //metoda przeszukuje pola tekstowe eventu i klas z nią związanych w celu znalezienia frazy podanej jako argument
        //public IQueryable<Event> GetEventsByPhrase(string phrase)
        //{
        //    var query = from e in context.Events
        //                where e.EventName.Contains(phrase) || e.Place.PlaceName.Contains(phrase) || e.Place.District.DistrictName.Contains(phrase) 
        //                || e.Description.Contains(phrase) || e.Place.StreetName.Contains(phrase) 
        //                || e.Category.CategoryName.Contains(phrase)
        //                select e;

        //    return query;
        //}

        //public IQueryable<Event> GetEventsByPrice(int price)
        //{
        //    var query = from e in context.Events
        //                   where e.Price == price
        //                   select e;

        //    return query;        
        //}

        public Event InsertEvent(Event newEvent)
        {
            return context.Events.Add(newEvent);
        }


        public void Save()
        {
            context.SaveChanges();
        }


        public void UpdateEvent(Event updatedEvent)
        {
            context.Entry(updatedEvent).State = EntityState.Modified;
        }
    }
}
