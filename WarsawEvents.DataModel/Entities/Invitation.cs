﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEvents.DataModel.Entities
{
    public class Invitation
    {

        //public User SendUser { get; set; }
        //public User ReceiveUser { get; set; }

        [Key, Column(Order = 0)]
        public int SendUserId { get; set; }

        [Key, Column(Order = 1)]
        public int ReceiveUserId { get; set; }
        public bool? IsAccepted { get; set; }
    }
}
