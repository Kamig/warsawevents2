﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEvents.DataModel.Entities
{
    public class Participant
    {
        [Key]
        [Column(Order = 0)]
        public int EventId { get; set; }
        [Key]
        [Column(Order = 1)]
        public int UserId { get; set; }

        public bool? IsOwner { get; set; }
        //public virtual Event Event { get; set; }
        public virtual User User { get; set; }
    }
}
