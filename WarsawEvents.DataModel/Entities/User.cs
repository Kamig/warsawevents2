﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarsawEvents.DataModel.Entities
{
    public class User : Entity
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<User> Inviters { get; set; }
        //public virtual ICollection<Grade> Grades { get; set; }
        //public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Participant> Participants { get; set; }
        public virtual ICollection<Invitation> SendInvitations { get; set; }
        public virtual ICollection<Invitation> ReceiveInvitations { get; set; }
    }

   /* public class UserDTO : Entity
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<User> Inviters { get; set; }
        //public virtual ICollection<Grade> Grades { get; set; }
        //public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Participant> Participants { get; set; }
    }*/
}
