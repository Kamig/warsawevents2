﻿using System.ComponentModel.DataAnnotations;

namespace WarsawEvents.DataModel.Entities
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
