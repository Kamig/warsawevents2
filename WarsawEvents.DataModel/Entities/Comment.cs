﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarsawEvents.DataModel.Entities
{
    public class Comment:Entity
    {
        public int EventId { get; set; }
        public int UserId { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }

        //public virtual Event Event { get; set; }
        public virtual User User { get; set; }
    }
}
