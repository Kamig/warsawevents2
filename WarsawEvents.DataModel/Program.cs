﻿using System;
using System.Collections.Generic;
using WarsawEvents.DataModel.Entities;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace WarsawEvents.DataModel
{
    class Program
    {
        static void Main(string[] args)
        {
            using (WarsawEventsContext cont = new WarsawEventsContext())
            {
                foreach (var e in cont.Events)
                    Console.WriteLine(e.EventName);
            }
        }
    }
}
