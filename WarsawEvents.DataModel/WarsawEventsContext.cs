﻿using System.Data.Entity;
using System;
using WarsawEvents.DataModel.Entities;

namespace WarsawEvents.DataModel
{

    public class WarsawEventsContext : DbContext, IDisposable
    {

        public DbSet<Category> Categorys { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Participant> Participants { get; set; }

        public WarsawEventsContext() : base("name=WarsawEventsConnectionString")
        {   //dropuje db gdy zmieni sie model
            Database.SetInitializer(new WarsawEventsInitializer());

            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false; //nie wywala przy serializacji obiektu
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //many-to-one
            modelBuilder.Entity<User>()
                       .HasMany(u => u.SendInvitations)
                       .WithRequired()
                       .HasForeignKey(i => i.SendUserId).WillCascadeOnDelete(false);

            //many-to-one 
            modelBuilder.Entity<User>()
                        .HasMany(u => u.ReceiveInvitations)
                        .WithRequired()
                        .HasForeignKey(i => i.ReceiveUserId).WillCascadeOnDelete(false);

            //one-to-many 
            //modelBuilder.Entity<Student>()
            //            .HasRequired<Standard>(s => s.Standard)
            //            .WithMany(s => s.Students)
            //            .HasForeignKey(s => s.StdId);
        }
    }
}
