﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WarsawEvents.DataModel.Entities;

namespace WarsawEvents.DataModel
{
    public static class FuncTool
    {
        public static Func<Event, object> GetSortFunc(string s)
        {
            s = s.ToLower();
            switch (s)
            {
                case "dacie":
                    return (Event e) => e.DateOfStart;
                case "cenie":
                    return (Event e) => e.Price;
                case "ilości uczestników":
                    return (Event e) => e.NumberOfParticipants;
                default:
                    return (Event e) => e.DateOfStart;
            }
        }

        public static Expression<Func<Event, bool>> GetPeriodFunc(string s)
        {
            s = s.ToLower();
            DateTime start,end;
            DateTime now = DateTime.Now;
            DateTime endOfWorld = now.AddYears(100);
            DateTime endOfWeekHelper = DateTime.Now.AddDays(7 - now.DayOfWeek.GetHashCode());

            switch (s)
            {
                case "wszystkie":
                    start = now;
                    end = endOfWorld;
                    break;
                case "dzisiejsze":
                    start = now;
                    end= new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                    break;
                case "obecny tydzień":
                    start = now;
                    end= new DateTime(endOfWeekHelper.Year, endOfWeekHelper.Month, endOfWeekHelper.Day, 23, 59, 59);
                    break;
                case "następny tydzień":
                    DateTime endOfWeek = new DateTime(endOfWeekHelper.Year, endOfWeekHelper.Month, endOfWeekHelper.Day, 23, 59, 59);
                    DateTime nextWeekHelper = endOfWeek.AddDays(1); //DateTime to strucktura nie klasa
                    start = new DateTime(nextWeekHelper.Year, nextWeekHelper.Month, nextWeekHelper.Day, 0, 0, 0);
                    nextWeekHelper = nextWeekHelper.AddDays(6);
                    end = new DateTime(nextWeekHelper.Year, nextWeekHelper.Month, nextWeekHelper.Day, 23, 59, 59);
                    break;
                default:
                    start = now;
                    end = endOfWorld;
                    break;
            }

            return (Event e) => (e.DateOfStart >= start && e.DateOfStart <= end);
        }
        //"Wszystkie", "Utworzyłem", "Dołączyłem", "Polubiłem", "Polecane"
        public static Expression<Func<Event, bool>> GetTypeOfEventsFunc(string s, User user)
        {
            if(s != null)
            s = s.ToLower();

            if (user == null)
            {
                if (s == "wszystkie")
                    return (Event e) => true;
                else
                    return (Event e) => false;
            }

            switch (s)
            {
                case "wszystkie":
                    return (Event e) => true;

                case "utworzyłem":
                    return (Event e) => e.Participants.Any(
                        (Participant p) => p.UserId == user.Id && p.IsOwner != null && p.IsOwner == true);

                case "dołączyłem":
                    return (Event e) => e.Participants.Any((Participant p) => p.UserId == user.Id);

                case "polubiłem":
                    return (Event e) => e.Likes.Any((Like l) => l.UserId == user.Id);

                case "polecane":

                    List<int> userList = new List<int>();

                    if (user.ReceiveInvitations != null)
                        foreach (Invitation inv in user.ReceiveInvitations)
                        {
                            if (inv.IsAccepted != null && inv.IsAccepted == true)
                                userList.Add(inv.SendUserId);
                        }
                    if (user.SendInvitations != null)
                        foreach (Invitation inv in user.SendInvitations)
                        {
                            if (inv.IsAccepted != null && inv.IsAccepted == true)
                                userList.Add(inv.ReceiveUserId);
                        }

                    List<Event> eventList = new List<Event>();

                    return (Event e) => e.Likes.Any((Like l) => userList.Contains(l.UserId) );
                default:
                    return (Event e) => true;
            }
        }


    }
}
