﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using WarsawEventsMVVM.Model;

namespace WarsawEventsMVVM.Converters
{
    class PlaceRankConv: IMultiValueConverter//przy dodawaniu oceny miejsca
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo ci)
        {
            Tuple<ComboBox, Place> tuple = new Tuple<ComboBox, Place>(
                (ComboBox)values[0], (Place)values[1]);
            return (object)tuple;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
