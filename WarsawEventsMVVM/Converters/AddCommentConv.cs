﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using WarsawEventsMVVM.Model;

namespace WarsawEventsMVVM.Converters
{
    public class AddCommentConv : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Tuple<TextBox, Event,Expander> tuple = new Tuple<TextBox, Event,Expander>(
                (TextBox)values[0], (Event)values[1],(Expander)values[2]);
            return (object)tuple;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
