﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace WarsawEventsMVVM.Converters
{
    class BoolToVisCollapsed: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {//na razie nie korzystamy
            if (value is Visibility && (Visibility)value == Visibility.Visible)
                return true;
            return false;
        }
    }
}
