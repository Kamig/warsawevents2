﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace WarsawEventsMVVM.Converters
{
    public class MultiValueConv : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo ci)
        {
            Tuple<PasswordBox, PasswordBox, Window> tuple = new Tuple<PasswordBox, PasswordBox, Window>(
                (PasswordBox)values[0], (PasswordBox)values[1],(Window)values[2]);
            return (object)tuple;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
