﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVVM.Model
{
    public class Invitation 
    {
        public int Id { get; set; }
        //public User SendUser { get; set; }
        //public User ReceiveUser { get; set; }
        public int SendUserId { get; set; }
        public int ReceiveUserId { get; set; }
        public bool ? IsAccepted { get; set; }
    }
}

