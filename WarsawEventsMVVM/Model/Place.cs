﻿
using System.Collections.Generic;

namespace WarsawEventsMVVM.Model
{
    public class Place
    {
        public int Id { get; set; }
        public string PlaceName { get; set; }
        public string StreetName { get; set; }
        public int Streetnumber { get; set; }
        public double Average { get; set; }

        //Foreign key for District
        public int DistrictId { get; set; }
        public virtual District District { get; set; }
        public virtual ICollection<Grade> Grades { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}
