﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVVM.Model
{
    public class Comment
    {
        public int Id { get; set; }
        public int EventId { get; set; }     
        public int UserId { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }

        //public virtual Event Event { get; set; }
        public virtual User User { get; set; }
    }
}
