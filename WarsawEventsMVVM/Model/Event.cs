﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WarsawEventsMVVM.Model
{
    public class Event: ObservableObject
    {
        private int id;
        private string eventName;
        private string description;
        private Category category;
        private  Place place;
        private double price;
        private int numberOfParticipants;
        private int placeId;
        private int categoryId;
        private DateTime dateOfStart;
        private ICollection<Comment> comments;
        private ICollection<Like> likes;
        private ICollection<Participant> participants;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                Set<int>(() => this.Id, ref id, value);
            }
        }

        public string EventName
        {
            get
            {
                return eventName;
            }
            set
            {
                Set<string>(() => this.EventName, ref eventName, value);
            }
        }
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                Set<string>(() => this.Description, ref description, value);
            }
        }

        public Category Category
        {
            get
            {
                return category;
            }
            set
            {
                Set<Category>(() => this.Category, ref category, value);
            }
        }

        public virtual Place Place
        {
            get
            {
                return place;
            }
            set
            {
                Set<Place>(() => this.Place, ref place, value);
            }
        }
        public int PlaceId
        {
            get
            {
                return placeId;
            }
            set
            {
                Set<int>(() => this.PlaceId, ref placeId, value);
            }
        }
        public int CategoryId
        {
            get
            {
                return categoryId;
            }
            set
            {
                Set<int>(() => this.CategoryId, ref categoryId, value);
            }
        }

        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                Set<double>(() => this.Price, ref price, value);
            }
        }

        public int NumberOfParticipants
        {
            get
            {
                return numberOfParticipants;
            }
            set
            {
                Set<int>(() => this.NumberOfParticipants, ref numberOfParticipants, value);
            }
        }

        public DateTime DateOfStart
        {
            get
            {
                return dateOfStart;
            }
            set
            {
                Set<DateTime>(() => this.DateOfStart, ref dateOfStart, value);
            }
        }

        public  ICollection<Comment> Comments
        {
            get
            {
                return comments;
            }
            set
            {
                Set<ICollection<Comment>>(() => this.Comments, ref comments, value);
            }
        }

        public virtual ICollection<Like> Likes
        {
            get
            {
                return likes;
            }
            set
            {
                Set<ICollection<Like>>(() => this.Likes, ref likes, value);
            }
        }

        public virtual ICollection<Participant> Participants
        {
            get
            {
                return participants;
            }
            set
            {
                Set<ICollection<Participant>>(() => this.Participants, ref participants, value);
            }
        }




        //public int Id { get; set; }
        //public string EventName { get; set; }
        //public string Description { get; set; }
        //public double Price { get; set; }
        //public DateTime DateOfStart { get; set; }
        //public int NumberOfParticipants { get; set; }
        //Foreign keys
        //public int PlaceId { get; set; }
        //public int CategoryId { get; set; }

        //public virtual Place Place { get; set; }
        //public virtual Category Category { get; set; }
        //public virtual ICollection<Comment> Comments { get; set; }
        //public virtual ICollection<Like> Likes { get; set; }
        //public virtual ICollection<Participant> Participants { get; set; }



    }//class
}
