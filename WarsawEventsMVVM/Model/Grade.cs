﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVVM.Model
{
    public class Grade
    {
        
        public int PlaceId { get; set; }
        
        public int UserId { get; set; }
       
        public int Value { get; set; }

        //public virtual Place Place { get; set; }
        //public virtual User User { get; set; }
    }
}
