﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WarsawEventsMVVM.Model;

namespace WarsawEventsMVVM.ViewModel
{
    
    public class AddViewModel: ViewModelBase
    {
        private Event _event;

        private Place _place;

        private ObservableCollection<District> _districts;
        private ObservableCollection<Category> _categories;
        private DateTime _dateTimePicker;
        private Category _selectedCategory;
        private District _selectedDistrict;
        private Event eventToEdit = null;




        public AddViewModel(ObservableCollection<District> districts, ObservableCollection<Category> categories, Event eventToEdit = null)
        {
            
            _event = new Event() { EventName = "Impreza" };

            _districts = districts;
            _categories = categories;

            _selectedDistrict = _districts[0];
            _selectedCategory = _categories[0];

            _event = new Event() { EventName = "Koncert", Description = "Fajna Impreza", Price = 15, NumberOfParticipants = 0 };
            _place = new Place() { PlaceName = "Klub", StreetName = "Warszawska", Streetnumber = 4 };
           
            _dateTimePicker = DateTime.Now.AddHours(1);

            if (eventToEdit != null)
                initializeEvent(eventToEdit);

            SendCommand = new RelayCommand<Window>(SendMethod);
        }

        private void initializeEvent(Event eventToEdit)
        {
            this.eventToEdit = eventToEdit;
            Event.Id = eventToEdit.Id;
            Event.Participants = eventToEdit.Participants;
            Event.Likes = eventToEdit.Likes;
            Event.Comments = eventToEdit.Comments;
            
            Event.EventName = eventToEdit.EventName;
            Event.Description = eventToEdit.Description;
            Event.Price = eventToEdit.Price;
            Event.NumberOfParticipants = eventToEdit.NumberOfParticipants;
           // Event.Participants = eventToEdit.Participants;

            _selectedCategory = _categories.Where(c => c.CategoryName == eventToEdit.Category.CategoryName).FirstOrDefault();
            _selectedDistrict = _districts.Where(d => d.DistrictName == eventToEdit.Place.District.DistrictName).FirstOrDefault();

            Place.PlaceName = eventToEdit.Place.PlaceName;
            Place.StreetName = eventToEdit.Place.StreetName;
            Place.Streetnumber = eventToEdit.Place.Streetnumber;

            _dateTimePicker = eventToEdit.DateOfStart;

        }

        public ICommand SendCommand { get; private set; }
        public bool IsSaved { get; set; } = false;
        public Event Event
        {
            get
            {
                return _event;
            }
            set
            {
                _event = value;
                RaisePropertyChanged("Event");
            }
        }

        public Place Place
        {
            get
            {
                return _place;
            }
            set
            {
                _place = value;
                RaisePropertyChanged("Place");
            }
        }

        public DateTime DateTimePicker
        {
            get
            {
                return _dateTimePicker;
            }
            set
            {
                _dateTimePicker = value;
                RaisePropertyChanged("DateTimePicker");
            }
        }

        public ObservableCollection<District> DistrictList
        {
            get
            {
                return _districts;
            }
        }

        public ObservableCollection<Category> CategoryList
        {
            get
            {
                return _categories;
            }
        }

        public Category SelectedCategory
        {
            get
            {
                return _selectedCategory;
            }
            set
            {
                _selectedCategory = value;
                RaisePropertyChanged("SelectedCategory");
            }
        }

        public District SelectedDistrict
        {
            get
            {
                return _selectedDistrict;
            }
            set
            {
                _selectedDistrict = value;
                RaisePropertyChanged("SelectedDistrict");
              
            }
        }

        private void SendMethod(Window window)
        {
            IsSaved = true;
            _event.DateOfStart = _dateTimePicker;
            _event.CategoryId = _selectedCategory.Id;
            _place.DistrictId = _selectedDistrict.Id;
            _event.Place = _place;
            window.Close();
        }//SendMethod
    }//class
}
