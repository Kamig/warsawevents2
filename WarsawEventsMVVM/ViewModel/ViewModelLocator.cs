/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:WarsawEventsMVVM"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.ServiceLocation;
using System.Windows;

namespace WarsawEventsMVVM.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<AddViewModel>();
            SimpleIoc.Default.Register<EventListViewModel>();
            SimpleIoc.Default.Register<PlaceListViewModel>();
            SimpleIoc.Default.Register<UserListViewModel>();
            SimpleIoc.Default.Register<LoggingViewModel>();

            Messenger.Default.Register<NotificationMessage>(this, NotifyUserMethod);
        }

        public MainViewModel MainViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public EventListViewModel EventListViewModel
        {
            get
            {
                return new EventListViewModel();
            }
        }

        public PlaceListViewModel PlaceListViewModel
        {
            get
            {
                return new PlaceListViewModel();
            }
        }

        public UserListViewModel UserListViewModel
        {
            get
            {
                //return ServiceLocator.Current.GetInstance<UserListViewModel>();
                return new UserListViewModel();
            }
        }

        public LoggingViewModel LoggingViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LoggingViewModel>();
            }
        }


        //    public static void CreateWindow()
        //{
        //   // var uniqueKey = System.Guid.NewGuid().ToString();
        //     var nonModalWindowVM = SimpleIoc.Default.GetInstance<AddViewModel>();

        //     var nonModalWindow = new AddNewEventWindow()
        //     {
        //          DataContext = nonModalWindowVM
        //     };
        //   // nonModalWindow.Closed += (sender, args) => SimpleIoc.Default.Unregister(uniqueKey);
        //    nonModalWindow.Show();
        // }


        private void NotifyUserMethod(NotificationMessage message)
        {
            MessageBox.Show(message.Notification);
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}