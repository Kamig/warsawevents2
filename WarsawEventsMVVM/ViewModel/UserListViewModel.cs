﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WarsawEventsMVVM.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using System.Windows;

namespace WarsawEventsMVVM.ViewModel
{
    public class UserListViewModel : ViewModelBase
    {
        private ObservableCollection<User> users;
        private string searchString = "";
        private HttpGetter httpGetter;
        private User userInfo;
        private ObservableCollection<string> filterUsersList;
        private string selectedFilterUsers;
        
        public UserListViewModel()
        {
            httpGetter = new HttpGetter();
           // users = new ObservableCollection<User>(httpGetter.GetUsers());
            // Messenger.Default.Send<NotificationMessage>(new NotificationMessage(users[0].UserName));
            SearchCommand = new RelayCommand(SearchMethod);
            InvitationCommand = new RelayCommand<User>(InvitationMethod);
            AcceptInvitationCommand = new RelayCommand<User>(AcceptInvitationMethod);
            userInfo = MainViewModel.UserInfo;
            //users.Remove(users.Where((User u) => u.Id == userInfo.Id).FirstOrDefault());
            InitializeFilterUsersList();
            RefreshUserList();
            
        }//PlaceListViewModel

        public ICommand SearchCommand { get; private set; }
        public ICommand InvitationCommand { get; private set; }
        public ICommand AcceptInvitationCommand { get; private set; }

    public User UserInfo
    {
        get
        {
            return userInfo;
        }
        set
        {
            userInfo = value;
            RaisePropertyChanged("UserInfo");
        }
    }

        public bool CanAccept
        {
            get
            {
                return UserInfo.IsLogged && SelectedFilterUsers == "Zapraszający";
            }
        }

        public bool CanInvite
        {
            get
            {
                return UserInfo.IsLogged && SelectedFilterUsers == "Wszyscy";
            }
        }

        public string SelectedFilterUsers
        {
            get
            {
                return selectedFilterUsers;
            }
            set
            {
                selectedFilterUsers = value;
                RaisePropertyChanged("SelectedFilterUsers");
                UseFilterUsers();
            }
        }

        public ObservableCollection<string>  FilterUsersList
        {
            get
            {
                return filterUsersList;
            }
            set
            {
                filterUsersList = value;
                RaisePropertyChanged("FilterUsersList");
            }
        }

        public ObservableCollection<User> UserList
    {
        get
        {
            return users;
        }
        set
        {
            users = value;
            RaisePropertyChanged("UserList");
        }
    }

    public string SearchString
    {
        get
        {
            return searchString;
        }
        set
        {
            searchString = value;
            RaisePropertyChanged("SearchString");
        }
    }

        private async void UseFilterUsers()
        {
            UserList =
                new ObservableCollection<User>(await httpGetter.GetUsers(SelectedFilterUsers, UserInfo.Id, SearchString));

            UserList.Remove(users.Where((User u) => u.Id == userInfo.Id).FirstOrDefault());
        }

        public async void RefreshUserList()
        {
            SelectedFilterUsers = FilterUsersList[0];

            if (SelectedFilterUsers != null && UserInfo.IsLogged == true)
                UserList = new ObservableCollection<User>(await httpGetter.GetUsers(SelectedFilterUsers, UserInfo.Id));
            else
            UserList = new ObservableCollection<User>(await httpGetter.GetUsers());
            if (UserList != null && userInfo.IsLogged)
            {
            UserList.Remove(users.Where((User u) => u.Id == userInfo.Id).FirstOrDefault());
            RaisePropertyChanged("UserList");
        }
        }
        private void InitializeFilterUsersList()
        {
            filterUsersList = new ObservableCollection<string>();
            filterUsersList.Add("Wszyscy");
            filterUsersList.Add("Zaproszeni");
            filterUsersList.Add("Zapraszający");
            filterUsersList.Add("Znajomi");
           


        }//InitializeFilterUsersList




         private async void InvitationMethod(User user)
        {
            if (userInfo.IsLogged == false)
                return;
            if(UserInfo.SendInvitations != null)
            foreach(Invitation inv in UserInfo.SendInvitations)
            {
                if (inv.ReceiveUserId == user.Id)
                {
                    if(inv.IsAccepted == true)
                        Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Jesteście znajomymi"));
                    else
                        Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Już wcześniej wysłano zaproszenie"));
                    return;
                }
            }
            if (UserInfo.ReceiveInvitations != null)
            foreach (Invitation inv in UserInfo.ReceiveInvitations)
            {
                if (inv.SendUserId == user.Id)
                {
                    if (inv.IsAccepted == true)
                        Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Jesteście znajomymi"));
                    else
                        Messenger.Default.Send<NotificationMessage>
                          (new NotificationMessage("Ten użytkownik już zaprosił Cię do znajomych, więc zostaliście znajomymi"));
                    AcceptInvitationMethod(user);
                    return;
                }
            }

            // Messenger.Default.Send<NotificationMessage>(new NotificationMessage(user.UserName));
            bool result =await httpGetter.PostInvitation(new Invitation()
            {  SendUserId = userInfo.Id, ReceiveUserId = user.Id, IsAccepted = false });
            if (result)
            {
                UserList = new ObservableCollection<User>(await httpGetter.GetUsers());
                Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Wysłano zaproszenie"));
            }
            else
                Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Już wcześniej wysłano zaproszenie"));

        }

        private async void AcceptInvitationMethod(User user)
        {
            if (userInfo.IsLogged == false)
                return;
           // Messenger.Default.Send<NotificationMessage>(new NotificationMessage(user.UserName));
            bool result =await httpGetter.PutInvitation(user.Id, userInfo.Id);
            if(result)
                UseFilterUsers();
        }

    private async void SearchMethod()
    {
            UserList = new ObservableCollection<User>( await httpGetter.GetUsers(SelectedFilterUsers, userInfo.Id, SearchString));
    }


}//class
}
