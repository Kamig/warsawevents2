﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Input;
using WarsawEventsMVVM.Model;

namespace WarsawEventsMVVM.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        private ViewModelBase _currentViewModel;

        static public User UserInfo { get; set; } = new User() { IsLogged = false };
        readonly static public EventListViewModel EventListViewModel = new EventListViewModel();
        readonly static public PlaceListViewModel PlaceListViewModel = new PlaceListViewModel();
        readonly static public UserListViewModel UserListViewModel = new UserListViewModel();

        public ViewModelBase CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                if (_currentViewModel == value)
                    return;
                _currentViewModel = value;
                RaisePropertyChanged("CurrentViewModel");
            }
        }

        public ICommand EventListViewCommand { get; private set; }
        public ICommand PlaceListViewCommand { get; private set; }
        public ICommand UserListViewCommand { get; private set; }
       

        public MainViewModel()
        {
            CurrentViewModel = MainViewModel.EventListViewModel;
            EventListViewCommand = new RelayCommand(() => ExecuteEventListViewCommand());
            PlaceListViewCommand = new RelayCommand(() => ExecutePlaceListViewCommand());
            UserListViewCommand = new RelayCommand(() => ExecuteUserListViewCommand());
         
        }

        private void ExecuteEventListViewCommand()
        {
            CurrentViewModel = MainViewModel.EventListViewModel;
        }

        private void ExecutePlaceListViewCommand()
        {
            CurrentViewModel = MainViewModel.PlaceListViewModel;
        }

        private void ExecuteUserListViewCommand()
        {
            CurrentViewModel = MainViewModel.UserListViewModel;
        }



    }//class
}