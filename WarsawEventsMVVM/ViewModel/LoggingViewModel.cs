﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WarsawEventsMVVM.Model;

namespace WarsawEventsMVVM.ViewModel
{
    public class LoggingViewModel : ViewModelBase
    {
        private User userInfo;
        private HttpGetter httpGetter;
        private bool isRegistration;
        private string errorMessage;

        public LoggingViewModel(User user, HttpGetter http)
        {
            LoginCommand = new RelayCommand<object>(LoginMethod);
            ChangeModeCommand = new RelayCommand(ChangeModeMethod);
            userInfo = user;
            httpGetter = http;
            errorMessage = "";
            isRegistration = false;
        }

        public ICommand LoginCommand { get; private set; }
        public ICommand ChangeModeCommand { get; private set; }

        public bool IsLogging//ptrzebne do bindingu
        {
            get
            {
                return !isRegistration;
            }
        }

        public bool IsRegistration
        {
            get
            {
                return isRegistration;
            }
            set
            {
                isRegistration = value;
                RaisePropertyChanged("IsRegistration");
            }
        }

        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
                RaisePropertyChanged("errorMessage");
            }
        }

        public User UserInfo
        {
            get
            {
                return userInfo;
            }
            set
            {
                UserInfo = value;
                RaisePropertyChanged("UserInfo");
            }
        }

        private void ChangeModeMethod()
        {
            IsRegistration = !IsRegistration;
            RaisePropertyChanged("IsLogging");
        }

        private async void LoginMethod(object o)
        {
            Tuple<PasswordBox, PasswordBox, Window> tuple = (Tuple<PasswordBox, PasswordBox, Window>)o;
            //MessageBox.Show(tuple.Item1.Password + " " + tuple.Item2.Password);
            string pass = tuple.Item1.Password,repeated=tuple.Item2.Password;

            if (UserInfo.UserName == null || UserInfo.UserName.Length < 5)
            {
                ErrorMessage = "za krótki login";
                return;
            }

            if(pass==null || pass.Length<5)
            {
                ErrorMessage = "za krótkie hasło";
                return;
            }

            if(IsRegistration && !ContainDigit(pass))
            {
                ErrorMessage = "hasło musi mieć cyfrę";
                return;
            }

            if(IsRegistration && pass != repeated)
            {
                ErrorMessage = "powtórz hasło";
                return;
            }

            
            pass = GetHashFromPassword(pass);
            UserInfo.Password = pass;
            if (!IsRegistration)//mamy logowanie
            {
                User user = await httpGetter.GetUser(UserInfo);
                if (user == null)
                {
                    ErrorMessage = "nie ma takiego loginu";
                    return;
                }
                if (user.Password != pass)
                {
                    ErrorMessage = "błędne hasło";
                    return;
                }
                userInfo.IsLogged = true;
                userInfo.Id = user.Id;
                userInfo.SendInvitations = user.SendInvitations;
                userInfo.ReceiveInvitations = user.ReceiveInvitations;
                tuple.Item3.Close();
            }
            else//rejestracja
            {
                User usr = await httpGetter.PostUser(userInfo);
                if (usr!=null)
                {
                    userInfo.IsLogged = true;
                    userInfo.Id = usr.Id;//potrzebne do placeView
                    userInfo.SendInvitations = usr.SendInvitations;
                    userInfo.ReceiveInvitations = usr.ReceiveInvitations;
                    tuple.Item3.Close();
                }
                else
                {
                    ErrorMessage = "login jest zajęty";
                    return;
                }
            }
        }

        private bool ContainDigit(string str)
        {
            foreach (char c in str)
                if (char.IsDigit(c))
                    return true;
            return false;
        }

        private string GetHashFromPassword(string pass)
        {            
            using (MD5 md5hash = MD5.Create())
            {
                pass = String.Join
                (
                    "",
                    from ba in md5hash.ComputeHash
                    (
                        Encoding.UTF8.GetBytes(pass)
                    )
                    select ba.ToString("x2")
                );
            }
            return pass;
        }
    }
}
