﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WarsawEventsMVVM.Model;

namespace WarsawEventsMVVM.ViewModel
{
     public class PlaceListViewModel: ViewModelBase
    {
        private ObservableCollection<Place> places;
        private ObservableCollection<string> sortFuncList;
        private ObservableCollection<int> gradeRange;
        private string selectedSortFunc;
        private string searchString = "";
        private HttpGetter httpGetter;
        private User userInfo;

        public PlaceListViewModel()
        {
            httpGetter = new HttpGetter();
            SearchCommand = new RelayCommand(SearchMethod);
            AddGradeCommand = new RelayCommand<object>(AddGradeMethod);
            UserInfo = MainViewModel.UserInfo;
            InitialiseLists();
        }//PlaceListViewModel

        public ICommand SearchCommand { get; private set; }
        public ICommand AddGradeCommand { get; private set; }

        public User UserInfo
        {
            get
            {
                return userInfo;
            }
            set
            {
                userInfo = value;
                RaisePropertyChanged("UserInfo");
            }
        }


        public ObservableCollection<Place> PlaceList
        {
            get
            {
                return places;
            }
            set
            {
                places = value;
                RaisePropertyChanged("PlaceList");
            }
        }

        public string SearchString
        {
            get
            {
                return searchString;
            }
            set
            {
                searchString = value;
                RaisePropertyChanged("SearchString");
            }
        }

        public ObservableCollection<string> SortFuncList
        {
            get
            {
                return sortFuncList;
            }
        }

        public ObservableCollection<int> GradeRange
        {
            get
            {
                return gradeRange;
            }
        }

        public string SelectedSortFunc
        {
            get
            {
                return selectedSortFunc;
            }
            set
            {
                selectedSortFunc = value;
                RaisePropertyChanged("SelectedSortFunc");
                if(selectedSortFunc== "Alfabetycznie")
                    PlaceList = new ObservableCollection<Place>(places.OrderBy(p=>p.PlaceName).ToList());
                else
                    PlaceList = new ObservableCollection<Place>(places.OrderByDescending(p => p.Average).ToList());
            }
        }

        private async void AddGradeMethod(object o)
        {
            Tuple<ComboBox, Place> tuple = (Tuple<ComboBox, Place>)o;
            int grade = (int)tuple.Item1.SelectedValue;
            Place place = tuple.Item2;
            Grade newGrade = new Grade() { PlaceId = place.Id, UserId = userInfo.Id, Value = grade };
            Place newPlace = await httpGetter.PostGrade(newGrade);
            if (newPlace==null)
            {
                MessageBox.Show("Juz oceniłeś to miejsce");
                return;
            }
            place.Average = newPlace.Average;
            SelectedSortFunc = selectedSortFunc;//update listy bo zmiana sredniej
        }

        private async void SearchMethod()
        {           
            PlaceList = new ObservableCollection<Place>(await httpGetter.GetPlaces(SearchString));
        }

        private async void InitialiseLists()
        {
            places = new ObservableCollection<Place>(await httpGetter.GetPlaces());
            sortFuncList = new ObservableCollection<string>() { "Alfabetycznie", "Ocenie" };
            gradeRange = new ObservableCollection<int>() { 1, 2, 3, 4, 5};
            RaisePropertyChanged(()=>this.PlaceList);
            RaisePropertyChanged(() => this.SortFuncList);
            
        }
    }//class
}//namespace
