﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WarsawEventsMVVM.Model;

namespace WarsawEventsMVVM.ViewModel
{
    public class EventListViewModel: ViewModelBase
    {

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        private ObservableCollection<Event> events;
        private ObservableCollection<District> districts;
        private ObservableCollection<Category> categories;
        private ObservableCollection<string> periodsOfTime;
        private ObservableCollection<string> typeOfEvents;
        private ObservableCollection<SortFunc> sortFuncs;
        private Category selectedCategory;
        private District selectedDistrict;
        private string selectedPeriodOfTime;
        private string selectedTypeOfEvents;
        private SortFunc selectedSortFunc;
        private Event selectedEvent;
        private MockEventRepository eventRepository;
        private string searchString = "";
        private int currentPage = 1;
        private User userInfo;
        private HttpGetter httpGetter;


        public EventListViewModel()
        {
            this.eventRepository = new MockEventRepository();
            //  AddEventCommand = new RelayCommand(
            //() =>
            //  ViewModelLocator.CreateWindow() );
            EnrollEventCommand= new RelayCommand<Event>(EnrollEventMethod);
            LikeEventCommand = new RelayCommand<Event>(LikeEventMedthod);
            AddCommentComment = new RelayCommand<object>(AddCommentMethod);
            LoggingCommand = new RelayCommand(LoggingMethod);
            LogOutCommand = new RelayCommand(LogOutMethod);
            FilterEventsCommand = new RelayCommand(FilterEventsMethod);
            RemoveEventCommand = new RelayCommand<Event>(RemoveEventMethod);
            AddEventCommand = new RelayCommand(AddEventMethod);
            EditEventCommand = new RelayCommand<Event>(EditEventMethod);
            PrevPageCommand = new RelayCommand(PrevPageMethod);
            NextPageCommand = new RelayCommand(NextPageMethod);

            httpGetter = new HttpGetter();
            InitializeDistricts();
            InitializeCategories();
            InitializePeriodsOfTime();
            InitializeTypeOfEvents();
            InitializeSortFuncs();

            //FilterEventsMethod();
            //GetEvents();
            events = new ObservableCollection<Event>();
            UserInfo = MainViewModel.UserInfo;

        }

        public ICommand EnrollEventCommand { get; private set; }
        public ICommand LikeEventCommand { get; private set; }
        public ICommand AddCommentComment { get; private set; }
        public ICommand LoggingCommand { get; private set; }
        public ICommand LogOutCommand { get; private set; }
        public ICommand FilterEventsCommand { get; private set; }
        public ICommand RemoveEventCommand { get; private set; }
        public ICommand AddEventCommand { get; private set; }
        public ICommand EditEventCommand { get; private set; }
        public ICommand PrevPageCommand { get; private set; }
        public ICommand NextPageCommand { get; private set; }

        public ObservableCollection<Event> EventList
        {
            get
            {
                //return new ObservableCollection<Event>(events.OrderBy(selectedSortFunc.SortBy).ToList());
                return events;
            }
        }

        public ObservableCollection<District> DistrictList
        {
            get
            {
                return districts;
            }
        }

        public ObservableCollection<Category> CategoryList
        {
            get
            {
                return categories;
            }
        }

        public ObservableCollection<string> PeriodOfTimeList
        {
            get
            {
                return periodsOfTime;
            }
        }

        public ObservableCollection<string> TypeOfEventsList
        {
            get
            {
                return typeOfEvents;
            }
        }

        public ObservableCollection<SortFunc> SortFuncList
        {
            get
            {
                return sortFuncs;
            }
        }

        public User UserInfo
        {
            get
            {
                return userInfo;
            }
            set
            {
                userInfo = value;
                RaisePropertyChanged("UserInfo");
            }
        }

        public string SearchString
        {
            get
            {
                return searchString;
            }
            set
            {
                searchString = value;
                CurrentPage = 1;
                RaisePropertyChanged("SearchString");
            }
        }

        public int CurrentPage
        {
            get
            {
                return currentPage;
            }
            set
            {
                currentPage = value;
                RaisePropertyChanged("CurrentPage");
            }
        }

        public Event SelectedEvent
        {
            get
            {
                return selectedEvent;
            }
            set
            {
                selectedEvent = value;
                RaisePropertyChanged("SelectedEvent");
            }
        }

        public Category SelectedCategory
        {
            get
            {
                return selectedCategory;
            }
            set
            {
                selectedCategory = value;
                CurrentPage = 1;
                RaisePropertyChanged("SelectedCategory");
                FilterEventsMethod();
            }
        }

        public District SelectedDistrict
        {
            get
            {
                return selectedDistrict;
            }
            set
            {
                selectedDistrict = value;
                CurrentPage = 1;
                RaisePropertyChanged("SelectedDistrict");
                FilterEventsMethod();
            }
        }

        public string SelectedPeriodOfTime
        {
            get
            {
                return selectedPeriodOfTime;
            }
            set
            {
                selectedPeriodOfTime = value;
                CurrentPage = 1;
                RaisePropertyChanged("SelectedPeriodOfTime");
                FilterEventsMethod();
            }
        }

        public string SelectedTypeOfEvents
        {
            get
            {
                return selectedTypeOfEvents;
            }
            set
            {
                selectedTypeOfEvents = value;
                CurrentPage = 1;
                RaisePropertyChanged("SelectedTypeOfEvents");
                FilterEventsMethod();
            }
        }

        public SortFunc SelectedSortFunc
        {
            get
            {
                return selectedSortFunc;
            }
            set
            {
                selectedSortFunc = value;
                CurrentPage = 1;
                RaisePropertyChanged("SelectedSortFunc");
                events = new ObservableCollection<Event>(events.OrderBy(selectedSortFunc.SortBy).ToList());
                this.RaisePropertyChanged(() => this.EventList);
            }
        }

        public bool IsMine
        {
            get
            {
               return selectedTypeOfEvents == "Utworzyłem" && UserInfo.IsLogged;
            }
        }

        private async void LikeEventMedthod(Event obj)
        {
            Like like = new Like() { EventId = obj.Id, UserId = userInfo.Id };
            bool result = await httpGetter.PostLike(like);
            if (result==false)
            {
                MessageBox.Show("Już lubisz to wydarzenie");
                return;
            }
            MessageBox.Show("Polubiłeś wydzarzenie: "+obj.EventName);
        }

        private async void EnrollEventMethod(Event obj)
        {
            Participant p = new Participant() { EventId = obj.Id, UserId = userInfo.Id, IsOwner=false };
            bool result = await httpGetter.PostParticipant(p);
            if (result==false)
            {
                MessageBox.Show("Już dołączyłeś do tego wydarzenia");
                return;
            }
            obj.NumberOfParticipants++;
            RaisePropertyChanged(() => this.EventList);
            MessageBox.Show("Dołączyłeś do wydarzenia");
        }

        private async void AddCommentMethod(object obj)
        {
            Tuple<TextBox, Event,Expander> tuple = (Tuple<TextBox, Event,Expander>)obj;
            Event e = tuple.Item2;
            TextBox tb = tuple.Item1;
            Comment com = new Comment() { Date = DateTime.Now, EventId = e.Id, Text = tb.Text, UserId=userInfo.Id,
                User = new User() { UserName = userInfo.UserName,  Id= userInfo.Id } };
            bool result = await httpGetter.PostComment(com);
            if (result)
            {
                e.Comments.Add(com);
                tb.Text = "";
                events = new ObservableCollection<Event>(events.OrderBy(selectedSortFunc.SortBy).ToList());
                RaisePropertyChanged(() => this.EventList);
                tuple.Item3.IsExpanded = false;
            }
        }

        private void LoggingMethod()
        {
            if (UserInfo.IsLogged)
                return;
            LoggingViewModel loggingViewModel = new LoggingViewModel(UserInfo,httpGetter);
            var win = new LoggingView();
            win.DataContext = loggingViewModel;
            win.ShowDialog();
            if (loggingViewModel.UserInfo.IsLogged)
            {
                RaisePropertyChanged("UserInfo");
                if (selectedTypeOfEvents != "Wszystkie")
                    FilterEventsMethod();
            }
            UserInfo.Password = "";

        }

        private void LogOutMethod()
        {
            UserInfo.IsLogged = false;
            UserInfo.Id = -1;
           // UserInfo.UserName = "";
            RaisePropertyChanged("UserInfo");
            RaisePropertyChanged("IsMine");
        }

        private async void FilterEventsMethod()
        {

            string sortCriterium = selectedSortFunc == null ? "Dacie" : selectedSortFunc.Name;
            if (selectedCategory != null && selectedDistrict != null && selectedPeriodOfTime != null)
                events = new ObservableCollection<Event>(await httpGetter.GetEvents(searchString, selectedCategory.CategoryName,
                    selectedDistrict.DistrictName, selectedPeriodOfTime, selectedTypeOfEvents, UserInfo.Id, sortCriterium, currentPage));
            // else
            //    events = new ObservableCollection<Event>(httpGetter.GetEvents());

            this.RaisePropertyChanged(() => this.EventList);

            //events = eventRepository.GetEvents(funcList);
            //this.RaisePropertyChanged(() => this.EventList);
            // Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Events Loaded."));
        }

        private async void RemoveEventMethod(Event selectedEvent)
        {

            bool result = await httpGetter.Delete(selectedEvent);
            if (result)
            {
                events.Remove(selectedEvent);
                //eventRepository.DeleteEvent(selectedEvent.Id);
                //eventRepository.Save();
                this.RaisePropertyChanged(() => this.EventList);
                Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Wydarzenie zostało usunięte."));
            }

        }

        private async void EditEventMethod(Event selectedEvent)
        {
            ObservableCollection<District> trueDistr = new ObservableCollection<District>(districts.Where(d => d.DistrictName != "Wszystkie"));
            ObservableCollection<Category> trueCateg = new ObservableCollection<Category>(categories.Where(c => c.CategoryName != "Wszystkie"));
            AddViewModel addViewModel = new AddViewModel(trueDistr, trueCateg, selectedEvent);
            var win = new AddNewEventWindow();
            win.DataContext = addViewModel;
            win.ShowDialog();

            // HttpGetter httpGetter = new HttpGetter();

            if (addViewModel.IsSaved)
            {
                bool result = await httpGetter.Put(addViewModel.Event);
                if (result)
                {
                    FilterEventsMethod();
                    Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Wydarzenie zostało edytowane."));
                }
            }
        }

        private async void AddEventMethod()
        {
            ObservableCollection<District> trueDistr = new ObservableCollection<District>(districts.Where(d => d.DistrictName != "Wszystkie"));
            ObservableCollection<Category> trueCateg = new ObservableCollection<Category>(categories.Where(c => c.CategoryName != "Wszystkie"));
            AddViewModel addViewModel = new AddViewModel(trueDistr, trueCateg, selectedEvent);
            var win = new AddNewEventWindow();
            win.DataContext = addViewModel;
            win.ShowDialog();

            //int newEventId = await httpGetter.Post(addViewModel.Event);
            //Event eventToAdd = addViewModel.Event;
            if (addViewModel.IsSaved)
            {
                int newEventId = await httpGetter.Post(addViewModel.Event);
                Event eventToAdd = addViewModel.Event;
                if ( newEventId >= 0)
                {
                    eventToAdd.Comments = new List<Comment>();
                    eventToAdd.Id = newEventId;
                    events.Add(eventToAdd);
                    Participant p = new Participant() { EventId = newEventId, UserId = userInfo.Id, IsOwner = true };
                    await httpGetter.PostParticipant(p);
                    FilterEventsMethod();
                    Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Wydarzenie zostało dodane."));
                }

            }

        }

        private void PrevPageMethod()
        {
            if (CurrentPage > 1)
            {
                CurrentPage--;
                FilterEventsMethod();
            }
        }

        private async void NextPageMethod()
        {
            currentPage++;
            string sortCriterium = selectedSortFunc == null ? "Dacie" : selectedSortFunc.Name;
            var collection = await httpGetter.GetEvents(searchString, selectedCategory.CategoryName,
                    selectedDistrict.DistrictName, selectedPeriodOfTime, selectedTypeOfEvents, UserInfo.Id, sortCriterium, currentPage);
            if(collection.Any() == true)
        {
                events = new ObservableCollection<Event>(collection);
                RaisePropertyChanged(() => this.EventList);
                CurrentPage = currentPage;
            }
            else
            {
                currentPage--;
            }
        }

        private async void InitializeDistricts()
        {

            districts = new ObservableCollection<District>(await httpGetter.GetDistricts());
            districts.Add(new District() { DistrictName = "Wszystkie" });
            RaisePropertyChanged("DistrictList");
        }//districts

        private async void InitializeCategories()
        {
            categories = new ObservableCollection<Category>(await httpGetter.GetCategories());
            categories.Add(new Category() { CategoryName = "Wszystkie" });
            RaisePropertyChanged("CategoryList");
        }//categories

        private void InitializePeriodsOfTime()
        {
            periodsOfTime = new ObservableCollection<string>()
            {
                "Wszystkie", "Dzisiejsze", "Obecny tydzień", "Następny tydzień"
            };
        }//periods

        private void InitializeTypeOfEvents()
        {
            typeOfEvents = new ObservableCollection<string>()
            {
                "Wszystkie", "Utworzyłem", "Dołączyłem", "Polubiłem", "Polecane"
            };
        }//typeOfEvents

        private void InitializeSortFuncs()
        {
            sortFuncs = new ObservableCollection<SortFunc>();
            sortFuncs.Add(new SortFunc("Dacie", (Event e) => e.DateOfStart));
            sortFuncs.Add(new SortFunc("Cenie", (Event e) => e.Price));
            sortFuncs.Add(new SortFunc("Ilości uczestników", (Event e) => e.NumberOfParticipants));
        }//sortFuncs

    }//class
}
