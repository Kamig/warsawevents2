﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarsawEventsMVVM.Model
{
    public class SortFunc
    {
        public string Name { get; set; }
        public Func<Event, object> SortBy { get; set; }

        public SortFunc(string name, Func<Event, object> sortBy)
        {
            Name = name;
            SortBy = sortBy;
        }

    }//class
}
